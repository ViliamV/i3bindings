# i3bindings
[![forthebadge](https://forthebadge.com/images/badges/made-with-python.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/no-ragrets.svg)](https://forthebadge.com)
## Goals
[i3](https://i3wm.org/) is a great windows manager but its config is pretty verbose.
I tried [bspwm](https://github.com/baskerville/bspwm) with its hotkey daemon [sxhkd](https://github.com/baskerville/sxhkd) and I much prefer that syntax.
That's why I wrote `i3bindings`.
It uses syntax similar to `sxhkd` for expansion of binding parameters, e.g. `$mod+{1-9,0} : workspace {1-10}` will become `bindsym $mod+1 workspace 1` and so on.
In addition, it has few more features.

## Syntax
`i3bindings` syntax has these special characters **-**, **/**, **:**, **::**, **#**, and a pair of **{}**.

**:** is a symbol for binding mapping.
`$mod+f : fullscreen toggle` will be transformed into `bindsym $mod+f fullscreen toggle`

**::** is a symbol for exec mapping.
`$mod+Return :: xterm` will be transformed into `bindsym $mod+Retrurn exec --no-startup-id xterm`

**/** is an alternative.
`$mod+h/Left : focus left` will create two lines of config:
`bindsym $mod+h focus left` and `bindsym $mod+Left focues left`

**{}** is a parameter list.
`$mod+{f,s} : {fullscreen,split} toggle` will create:
`bindsym $mod+f fullscreen toggle`
`bindsym $mod+s split toggle`

Note 1: you can use **/** inside parameter list, e.g. `$mod+{h/Left,j/Down,k/Up,l/Right} : ...`

Note 2: you can use empty argument inside parameter list, e.g. `$mod+{,Shift+}{h/Left,j/Down,k/Up,l/Right} : {focus,move} {left,down,up,right}`

**-** is a range expansion.
`$mod+{,Shift+}{1-9,0} : {,move container to }workspace {1-10}`

Note that this one line will create 20 lines of i3 configuration.

**#** is a comment. Lines starting with # will be ignored.
## Usage
To use `i3bindings` I recommend keeping the bindings and the rest of i3 config file separate.
The real config file that i3 uses can be created like this:
```sh
    cd ~/.config/i3
    rm config 2>/dev/null
    cat config_base > config
    ./i3bindings bindings.txt >> config
```
## Examples
My personal bindings file looks like this:
```config
    $mod+{,Shift+}{h/Left,j/Down,k/Up,l/Right} : {focus,move} {left,down,up,right}
    $mod+Control+Shift+h/Left : resize shrink width 5px or 5ppt
    $mod+Control+Shift+j/Down : resize grow height 5px or 5ppt
    $mod+Control+Shift+k/Up : resize shrink height 5px or 5ppt
    $mod+Control+Shift+l/Right : resize grow width 5px or 5ppt

    $mod+{,Shift+}{1-9,10} : {,move container to }workspace {1-10}
    # $mod+Control+Shift {1,2,3,4,5,6,7,8,9} : move container to workspace {}; workspace {}
    $mod+grave/Escape : workspace back_and_forth
    $mod+{,Shift+}Tab : workspace {next,prev}
    $mod+{s,f} : {split,fullscreen} toggle

    $mod+Return :: terminal
    $mod+space :: rofi -show drun
    $mod+{m,comma,period,slash} :: terminal-fullscreen {output_fzf,passwords_fzf,snippets_fzf,clipboard_fzf}
    $mod+Shift+q :: terminal-fullscreen power_fzf
    $mod+p :: scrot ~/Pictures/Screenshots/%Y-%m-%d_%H:%M:%S.png && notify-send "Screenshot taken"
    Print :: scrot ~/Pictures/Screenshots/%Y-%m-%d_%H:%M:%S.png && notify-send "Screenshot taken"
    $mod+{c,t,g} :: {gsiplecal, thunar, geany}
    $mod+x : [instance=\"calculator\"] scratchpad show; [instance=\"calculator\"] move position center

    Caps_Lock :: setxkbmap -option "caps:escape" && xdotool key Caps_Lock
    Control+Shift+q : kill
    $mod+Shift+r : restart
    # vim:filetype=config
```

The corresponding i3 config file is this one:
```i3
    bindsym $mod+h focus left
    bindsym $mod+Left focus left
    bindsym $mod+j focus down
    bindsym $mod+Down focus down
    bindsym $mod+k focus up
    bindsym $mod+Up focus up
    bindsym $mod+l focus right
    bindsym $mod+Right focus right
    bindsym $mod+Shift+h move left
    bindsym $mod+Shift+Left move left
    bindsym $mod+Shift+j move down
    bindsym $mod+Shift+Down move down
    bindsym $mod+Shift+k move up
    bindsym $mod+Shift+Up move up
    bindsym $mod+Shift+l move right
    bindsym $mod+Shift+Right move right
    bindsym $mod+Control+Shift+h resize shrink width 5px or 5ppt
    bindsym $mod+Control+Shift+Left resize shrink width 5px or 5ppt
    bindsym $mod+Control+Shift+j resize grow height 5px or 5ppt
    bindsym $mod+Control+Shift+Down resize grow height 5px or 5ppt
    bindsym $mod+Control+Shift+k resize shrink height 5px or 5ppt
    bindsym $mod+Control+Shift+Up resize shrink height 5px or 5ppt
    bindsym $mod+Control+Shift+l resize grow width 5px or 5ppt
    bindsym $mod+Control+Shift+Right resize grow width 5px or 5ppt
    bindsym $mod+1 workspace 1
    bindsym $mod+2 workspace 2
    bindsym $mod+3 workspace 3
    bindsym $mod+4 workspace 4
    bindsym $mod+5 workspace 5
    bindsym $mod+6 workspace 6
    bindsym $mod+7 workspace 7
    bindsym $mod+8 workspace 8
    bindsym $mod+9 workspace 9
    bindsym $mod+10 workspace 10
    bindsym $mod+Shift+1 move container to workspace 1
    bindsym $mod+Shift+2 move container to workspace 2
    bindsym $mod+Shift+3 move container to workspace 3
    bindsym $mod+Shift+4 move container to workspace 4
    bindsym $mod+Shift+5 move container to workspace 5
    bindsym $mod+Shift+6 move container to workspace 6
    bindsym $mod+Shift+7 move container to workspace 7
    bindsym $mod+Shift+8 move container to workspace 8
    bindsym $mod+Shift+9 move container to workspace 9
    bindsym $mod+Shift+10 move container to workspace 10
    bindsym $mod+grave workspace back_and_forth
    bindsym $mod+Escape workspace back_and_forth
    bindsym $mod+Tab workspace next
    bindsym $mod+Shift+Tab workspace prev
    bindsym $mod+s split toggle
    bindsym $mod+f fullscreen toggle
    bindsym $mod+x [instance=\"calculator\"] scratchpad show; [instance=\"calculator\"] move position center
    bindsym Control+Shift+q kill
    bindsym $mod+Shift+r restart
    bindsym $mod+Return exec --no-startup-id terminal
    bindsym $mod+space exec --no-startup-id rofi -show drun
    bindsym $mod+m exec --no-startup-id terminal-fullscreen output_fzf
    bindsym $mod+comma exec --no-startup-id terminal-fullscreen passwords_fzf
    bindsym $mod+period exec --no-startup-id terminal-fullscreen snippets_fzf
    bindsym $mod+slash exec --no-startup-id terminal-fullscreen clipboard_fzf
    bindsym $mod+Shift+q exec --no-startup-id terminal-fullscreen power_fzf
    bindsym $mod+p exec --no-startup-id scrot ~/Pictures/Screenshots/%Y-%m-%d_%H:%M:%S.png && notify-send "Screenshot taken"
    bindsym Print exec --no-startup-id scrot ~/Pictures/Screenshots/%Y-%m-%d_%H:%M:%S.png && notify-send "Screenshot taken"
    bindsym $mod+c exec --no-startup-id gsiplecal
    bindsym $mod+t exec --no-startup-id  thunar
    bindsym $mod+g exec --no-startup-id  geany
    bindsym Caps_Lock exec --no-startup-id setxkbmap -option "caps:escape" && xdotool key Caps_Lock
```

## TODO:
* Support for this type of binding: `bindsym $mod+Control+Shift 1 move container to workspace 1; workspace 1`
